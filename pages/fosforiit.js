import { useRouter } from "next/router";
import styles from "../styles/Games.module.css";
import Footer from "../components/Footer";
import BgImage from "../components/BgImage";
import Container from "../components/Container";

export default function PhosphoritePage(props) {
  const router = useRouter();

  return (
    <Container title="Fosforiit">
      <main className={styles.main}>
        <h1 className={styles.title}>{props.title}</h1>
        {/* <p className={styles.content} style={{ margin: "-0.75rem 0" }}>
          {props.content}
        </p> */}
        <div style={{ width: "80%", paddingTop: "2rem", marginBottom: "1rem" }}>
          <div className={styles.frame_container}>
            <iframe
              className={styles.responsive_iframe}
              style={{ paddingBottom: "1rem" }}
              src="https://www.youtube.com/embed/bNUP7iSgopw"
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
          </div>

          <div className={styles.frame_container}>
            <iframe
              src={props.gameUrl}
              className={styles.responsive_iframe}></iframe>
          </div>
        </div>

        <div className={styles.button_grid}>
          <div>
            <button
              onClick={() => router.reload()}
              className={styles.button_link}
              align="center">
              Alusta uuesti
            </button>
          </div>
        </div>
      </main>
      <Footer mineheritage />

      <BgImage></BgImage>
    </Container>
  );
}

export async function getStaticProps(context) {
  return {
    props: {
      title: "Fosforiit",
      content: "Vaata videot ja testi teadmisi.",
      gameUrl: "https://learningapps.org/watch?v=p3afkwyr521",
    },
  };
}
