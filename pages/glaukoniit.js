import styles from "../styles/Sammalloom.module.css";
import Footer from "../components/Footer";
import BgImage from "../components/BgImage";
import ButtonGrid from "../components/ButtonGrid";
import Container from "../components/Container";
import Link from "next/link";

export default function GlaukoniitPage(props) {
  return (
    <Container title={props.title}>
      <main className={styles.main}>
        <h1 className={styles.title}>{props.title}</h1>
        {/*      <p className={styles.content}>{props.subtitle1}</p>
        <p className={styles.content}>{props.subtitle2}</p> */}
        <div className={styles.main_content}>
          <div className={styles.text_column}>
            <p>
              <b>Asukoht: </b>Põhja-Eestiklint.
            </p>
            <p>
              <b>Vanus: </b>470-460 Ma
            </p>
            <p>
              Merelise lubimuda settimine Eesti alal algas Ordoviitsiumi ajastul
              ligikaudu 470 miljonit aastat tagasi ning kestis kuni Siluri
              ajastu lõpuni (u 415 mln a tagasi). Aja jooksul sete tihenes ning
              kivistus — tekkis lubjakivi. Lubjakivi koosneb valdavalt
              kaltsiumkarbonaadist (mineraal kaltsiit), kuid lisandina võib
              selles leiduda ka kvartsiterasid ja savimineraale nagu näiteks
              glaukoniit.
            </p>
            <p>
              Galukoniit on roheline savimineraal, mida võib leiduda liiva- või
              lubjakivides. Kui glaukoniidi sisaldus tõuseb kivimis üle 50% on
              tegemist glaukoniitkivimiga. Sarnaselt lubjakiviga tekib
              glaukoniit merelise settimise tulemusena. Tal on ka erinevaid
              kasutusvaldkondi, näiteks väetise või värvi tootmisel.
            </p>

            <p>
              Karbonaatsete setete moodustumisel oli tähtis roll mereelustikul,
              nii mikroskoopilistel organismidel kui suurematel lubiskeletiga
              loomadel, kelle kivistisi võib Eesti lubjakividest sageli leida.
              Lubimuda settimine madalmeres ei toimunud kunagi ühtlaselt ja
              pidevalt. Seetõttu on tüüpilised Eesti lubjakivid kihilised, iga
              allpool asetsev kiht vanem kui selle peal asuvad kihid.
            </p>
          </div>

          <div className={styles.image_column}>
            {props.images.map((image, i) => {
              return (
                <div key={i}>
                  <img
                    src={image.src}
                    className={styles.text_image}
                    alt={image.alt}
                  />
                  <p className={styles.caption}>{image.caption}</p>
                </div>
              );
            })}
          </div>
        </div>
        <ButtonGrid>
          <div>
            <Link href="/sammalloom" rel="nofollow noopener">
              <button className={styles.button_link} align="center">
                Sammalloomast lähemalt
              </button>
            </Link>
          </div>
          <div>
            <Link href="/puursudamik" rel="nofollow noopener">
              <button className={styles.button_link} align="center">
                Puursüdamiku sees
              </button>
            </Link>
          </div>
        </ButtonGrid>
      </main>
      <Footer />

      <BgImage></BgImage>
    </Container>
  );
}

export async function getStaticProps(context) {
  return {
    props: {
      title: "Roheline kivi - glaukoniit",
      images: [
        {
          src: "img/ohikud/image010.jpg",
          alt: "Glaukoniit lubjakivi",
          caption: "Autor: Carina Potagin",
        },
        {
          src: "img/ohikud/image012.jpg",
          alt: "Maapinna kihid",
          caption: "Allikas: http://www.geo.ut.ee/~raivo/Ontika.html",
        },
      ],
    },
  };
}
