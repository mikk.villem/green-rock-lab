import Link from "next/link";
import matter from "gray-matter";
import styles from "../styles/Home.module.css";
import Footer from "../components/Footer";
import BgImage from "../components/BgImage";
import Container from "../components/Container";

export default function Home({ data, title }) {
  const RealData = data.map((game) => matter(game));
  const GridItems = RealData.map((gridItem) => gridItem.data);

  return (
    <Container title="Avaleht">
      <main className={styles.main}>
        <h1 className={styles.title}>{title}</h1>

        <div className={styles.grid}>
          {GridItems.map((game, i) => (
            <Link key={i} href={`/games/${game.slug}`}>
              <a className={styles.card}>
                <h3>{game.title}</h3>
                <p>{game.description}</p>
                <img src={`${game.qr}`} className={styles.qr}></img>
              </a>
            </Link>
          ))}
        </div>
      </main>
      <Footer />

      <BgImage></BgImage>
    </Container>
  );
}

export async function getStaticProps() {
  const fs = require("fs");

  const files = fs.readdirSync(`${process.cwd()}/content`, "utf-8");

  const games = files.filter((fn) => fn.endsWith(".md"));

  const data = games.map((game) => {
    const path = `${process.cwd()}/content/${game}`;
    const rawContent = fs.readFileSync(path, {
      encoding: "utf-8",
    });

    return rawContent;
  });

  return {
    props: {
      data: data,
      title: "Vali mäng",
    },
  };
}
