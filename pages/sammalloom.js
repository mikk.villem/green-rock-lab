import styles from "../styles/Sammalloom.module.css";
import Footer from "../components/Footer";
import BgImage from "../components/BgImage";
import ButtonGrid from "../components/ButtonGrid";
import Container from "../components/Container";
import Link from "next/link";

export default function SammalloomPage(props) {
  return (
    <Container title="Sammalloomast">
      <main className={styles.main}>
        <h1 className={styles.title}>{props.title}</h1>
        {/*      <p className={styles.content}>{props.subtitle1}</p>
        <p className={styles.content}>{props.subtitle2}</p> */}
        <div className={styles.main_content}>
          <div className={styles.text_column}>
            <p>
              <b>Asukoht: </b>Keila paemurd, Ülem-Ordoviitsiumi lubjakivi, Keila
              lade.
            </p>
            <p>
              <b>Vanus: </b>ca 455 Ma
            </p>
            <p>
              Vanimad sammalloomade kivistised on leitud Ordoviitsiumi
              kivimitest, seega ulatub kogu rühma vanus umbes 470 miljoni
              aastani. See pole kuigi iidne iga, võrreldes teiste suurte
              hõimkondadega (sh. lülijalgsete ja limustega), kes on elanud juba
              Kambriumi ajastu alguses ning edestavadsammalloomi ligikaudu 70
              miljoni aastaga. On arvatud, et sammalloomade elutegevuseks ei ole
              tugevat välisskeletti ilmtingimata vaja ja seetõttu võisid nad
              pehmekehaliste loomakestena elada juba tunduvalt varasemal ajal.
              Paraku on alla millimeetri suuruse pehme kehaga isendi kivistumise
              tõenäosus väga väike, seetõttu on tavaliste, paleontoloogiliste
              meetoditega kaheldav seda väidet tõestada.
            </p>
            <p>
              Ürgsete merede kunagised asukad, fossiilsed sammalloomad, olid osa
              madalmere põhjakooslusest. Sammalloomade kivististe levikusageduse
              järgi võis neid merepõhjas olla märkimisväärselt palju. Kohati on
              toeste tihe paigutus tekitanud isegi korallriffe meenutavaid
              kõrgendeid. Sammalloomade toese varieeruv ehitus viitab aga väga
              heale kohastumusele. Jälgides erisuguste toese põhitüüpide levikut
              kivimikihtides, on võimalik saada aimu iga kihi
              kujunemistingimuste kohta. Nii on massiivsed ja poolkerajad
              kolooniad olnud omased madala- ja liikuvaveelises keskkonnas
              kujunenud kihtidele. Mida väiksem on olnud vee liikuvus, seda
              rohkem võib merepõhjas leida püstiseid põõsasjaid kolooniad.
            </p>

            <p>
              Üle saja liigi. Sammalloomad on tuntud eriti keerulise taksonoomia
              poolest. Väikeste mõõtmete, peene morfoloogia ning raskesti
              eristatavate tunnuste tõttu nõuab nende määramine suurt kogemust.
              Et näha sammallooma detaile, jääb tihti ka korralikust luubist
              väheks – parem on kasutada hea suurendusega mikroskoopi. Paljusid
              liike aitavad täpsemalt määrata kolooniast võetud spetsiaalsed
              preparaadid: 0,3 mm paksused risti- ja pikilõikesõhikud, kus on
              võimalik jälgida kolooniate siseehituse olulisi tunnuseid.
            </p>
            <p>Eesti Loodus 2006/9</p>
          </div>

          <div className={styles.image_column}>
            {props.images.map((image, i) => {
              return (
                <div key={i}>
                  <img
                    src={image.src}
                    className={styles.text_image}
                    alt={image.alt}
                  />
                  <p className={styles.caption}>{image.caption}</p>
                </div>
              );
            })}
          </div>
        </div>
        <ButtonGrid>
          <div>
            <Link href="/glaukoniit" rel="nofollow noopener">
              <button className={styles.button_link} align="center">
                Roheline kivi - glaukoniit
              </button>
            </Link>
          </div>
          <div>
            <Link href="/puursudamik" rel="nofollow noopener">
              <button className={styles.button_link} align="center">
                Puursüdamiku sees
              </button>
            </Link>
          </div>
        </ButtonGrid>
      </main>
      <Footer />
      <div style={{}}></div>
      <BgImage></BgImage>
    </Container>
  );
}

export async function getStaticProps(context) {
  return {
    props: {
      title: "Sammalloomast lähemalt",
      subtitle1:
        "Asukoht: Keila paemurd, Ülem-Ordoviitsiumi lubjakivi, Keila lade.",
      images: [
        {
          src: "img/ohikud/image001.jpg",
          alt: "Sammalloom palju",
          caption: "Autor: Carina Potagin",
        },
        {
          src: "img/ohikud/image004.jpg",
          alt: "Sammalloom 3cm",
          caption: "Autor: Gennadi Baranov",
        },
        {
          src: "img/ohikud/image007.jpg",
          alt: "Sammalloom 5cm",
          caption: "Autor: Gennadi Baranov",
        },
      ],
    },
  };
}
