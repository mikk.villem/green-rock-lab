import styles from "../styles/Sammalloom.module.css";
import Footer from "../components/Footer";
import BgImage from "../components/BgImage";
import ButtonGrid from "../components/ButtonGrid";
import Container from "../components/Container";
import Link from "next/link";

export default function GlaukoniitPage(props) {
  return (
    <Container title={props.title}>
      <main className={styles.main}>
        <h1 className={styles.title}>{props.title}</h1>
        {/*      <p className={styles.content}>{props.subtitle1}</p>
        <p className={styles.content}>{props.subtitle2}</p> */}

        {props.images.map((image, i) => {
          return (
            <div key={i} style={{ width: "80%", padding: "2rem 0 0.5rem 0" }}>
              <img
                src={image.src}
                className={styles.title_image}
                alt={image.alt}
              />
              <p className={styles.caption}>{image.caption}</p>
            </div>
          );
        })}
        <div>
          <div className={styles.text_column} style={{ paddingLeft: "2rem" }}>
            <p>
              Ubja puursüdamik on pärit Lääne-Virumaalt Ubja küla lähistelt.
              Puursüdamik ulatub kuni ca 26 meetrini, sisaldades endas vaid
              Ordoviitsiumi ajastu kivimeid, mille vanus ulatub kuni 460 miljoni
              aastani.
            </p>
            <p>
              Puursüdamiku esimeses pooles on näha Viivikonna kihistut, mis
              sisaldab vaheldumisi põlevkivi ja lubjakivi kihte. Tegemist on
              sama kihistuga, millest kaevandatakse Ida-Virumaal põlevkivi. Kuid
              Ubja puuraugus on nende kihtide paksus palju väiksem ning materjal
              orgaanikavaesem.
            </p>

            <p>
              Järgnev Kõrgekalda kihistu koosneb peamiselt savikast lubjakivist
              ehk merglist, mis võib vähesel määral ka kukersiiti sisaldada.
              Antud lubjakivi ei ole ilmastikukindel ning ei sobi seega
              ehitusmaterjaliks.
            </p>
            <p>
              Puursüdamiku sügavaimas osas olevas Väo kihistus muutub savi
              osakaal lubjakivis palju madalaks. Tegemist on väärtusliku
              lubjakiviga, mida on aastakümneid ehituslubjakivina kasutatud.{" "}
            </p>
          </div>
        </div>
        <ButtonGrid>
          <div>
            <Link href="/sammalloom" rel="nofollow noopener">
              <button className={styles.button_link} align="center">
                Sammalloomast lähemalt
              </button>
            </Link>
          </div>
          <div>
            <Link href="/glaukoniit" rel="nofollow noopener">
              <button className={styles.button_link} align="center">
                Roheline kivi - glaukoniit
              </button>
            </Link>
          </div>
        </ButtonGrid>
      </main>
      <Footer />

      <BgImage></BgImage>
    </Container>
  );
}

export async function getStaticProps(context) {
  return {
    props: {
      title: "Ubja puursüdamik",
      images: [
        {
          src: "img/ohikud/image002.png",
          alt: "Eesti kaart",
          caption:
            "Joonis. Eesti kaart, kus on tärniga tähistatud Ubja asukoht. (Google maps)",
        },
      ],
    },
  };
}
