import Head from "next/head";
import matter from "gray-matter";
import Link from "next/link";
import { useRouter } from "next/router";
import styles from "../../styles/Games.module.css";
import Footer from "../../components/Footer";
import BgImage from "../../components/BgImage";
import Container from "../../components/Container";
import ButtonGrid from "../../components/ButtonGrid";

export default function GameTemplate({ content, data, slug }) {
  const frontmatter = data;
  const router = useRouter();

  return (
    <Container>
      <Head>
        <title>Green Rock Lab | {frontmatter.title}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>{frontmatter.title}</h1>
        <p className={styles.content}>{content}</p>
        <div style={{ width: "100%", paddingTop: "2rem" }}>
          <iframe src={frontmatter.url} className={styles.iframe}></iframe>
        </div>
        <ButtonGrid>
          <div>
            <Link href="/" rel="nofollow noopener">
              <button className={styles.button_link} align="center">
                Tagasi valikusse
              </button>
            </Link>
          </div>
          <div>
            <button
              onClick={() => router.reload()}
              className={styles.button_link}
              align="center">
              Alusta uuesti
            </button>
          </div>
        </ButtonGrid>
      </main>
      <Footer />

      <BgImage></BgImage>
    </Container>
  );
}

GameTemplate.getInitialProps = async (context) => {
  const { slug } = context.query;
  const content = await import(`../../content/${slug}.md`);
  const data = matter(content.default);
  return { ...data, slug };
};
