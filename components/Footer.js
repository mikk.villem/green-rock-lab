import React from "react";
import styles from "../styles/Footer.module.css";

const Footer = (props) => {
  return (
    <footer className={styles.footer}>
      <img
        key={0}
        src="../img/GreenRockLab.svg"
        alt="Green Rock Lab Logo"></img>
      <img key={1} src="../img/EIT.svg" alt="EIT Raw Materials Logo"></img>
      <img key={2} src="../img/EU.svg" alt="EU funding Logo"></img>
      <img
        key={3}
        src="../img/TalTech_logo_valge.svg"
        alt="TalTech Geoloogia Instituut Logo"></img>

      {props.mineheritage ? (
        <img
          key={4}
          src="../img/mineheritage.png"
          alt="Mineheritages Logo"></img>
      ) : (
        ""
      )}
    </footer>
  );
};

export default Footer;
