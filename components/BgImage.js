import React from "react";
import styles from "../styles/BgImage.module.css";

const BgImage = () => {
  return <img src="../img/bg_room.jpg" className={styles.bg} />;
};

export default BgImage;
