import React from "react";
import styles from "../styles/Container.module.css";
import Head from "next/head";

const Container = ({ children, title }) => {
  return (
    <div className={styles.container}>
      <Head>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
        />
        <meta name="mobile-web-app-capable" content="yes"></meta>
        <title>Green Rock Lab | {title}</title>
        <link rel="icon" href="/favicon.ico" />
        <link
          href="/icons/favicon-16.png"
          rel="icon"
          type="image/png"
          sizes="16x16"
        />
        <link
          href="/icons/favicon-32.png"
          rel="icon"
          type="image/png"
          sizes="32x32"
        />
        <link
          href="/icons/apple-touch-icon.png"
          rel="apple-touch-icon"
          type="image/png"
          sizes="128x128"
        />
        <link
          href="/icons/favicon-192.png"
          rel="icon"
          type="image/png"
          sizes="192x192"
        />
      </Head>

      {children}
    </div>
  );
};

export default Container;
