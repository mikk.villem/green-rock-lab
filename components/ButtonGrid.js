import React from "react";
import styles from "../styles/ButtonGrid.module.css";

const ButtonGrid = ({ children }) => {
  return <div className={styles.button_grid}>{children}</div>;
};

export default ButtonGrid;
