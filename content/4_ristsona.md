---
title: "Ristsõna"
slug: "4_ristsona"
url: "https://learningapps.org/watch?v=pug7oun2v21"
qr: "/img/qr/qrcode_6.png"
---

Vasta küsimustele ja täida ristsõna.
