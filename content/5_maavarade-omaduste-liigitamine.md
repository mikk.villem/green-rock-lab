---
title: "Maavarade omaduste liigitamine"
slug: "5_maavarade-omaduste-liigitamine"
url: "https://learningapps.org/watch?v=pr2osxxb217"
qr: "/img/qr/qrcode_8.png"
---

Paiguta maavarad õigesse rühma.
