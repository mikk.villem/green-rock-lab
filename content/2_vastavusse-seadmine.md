---
title: "Vastavusse seadmine"
slug: "2_vastavusse-seadmine"
url: "https://learningapps.org/watch?v=pchu1p0v320"
qr: "/img/qr/qrcode_2.png"
---

Ühenda omavahel maavara ja toode, milleks saab antud maavara kasutada.
