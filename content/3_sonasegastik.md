---
title: "Sõnasegastik"
slug: "3_sonasegastik"
url: "https://learningapps.org/watch?v=ps323htsn20"
qr: "/img/qr/qrcode_5.png"
---

Leia sõnasegadikust maavarade ja nende kasutamisega seotud sõnad.
